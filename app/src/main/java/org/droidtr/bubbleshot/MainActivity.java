package org.droidtr.bubbleshot;

import android.app.*;
import android.os.*;
import android.webkit.*;
import android.widget.*;

public class MainActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		LinearLayout.LayoutParams defparam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
		LinearLayout ll = new LinearLayout(this);
		WebView w = new WebView(this);
		setContentView(ll);
		w.setLayoutParams(defparam);
		ll.addView(w);
		w.getSettings().setJavaScriptEnabled(true);
        w.getSettings().setDomStorageEnabled(true);
		w.getSettings().setAllowFileAccess(true);
		w.getSettings().setAllowFileAccessFromFileURLs(true);
		w.setFocusable(false);
		w.setLongClickable(false);
		w.loadUrl("file:///android_asset/bubbleshot/index.html");
    }
}
